<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>GESTAO_LICENCAS_STATUS_CONCLUIDA</fullName>
        <field>STATUS__c</field>
        <literalValue>Aprovada</literalValue>
        <name>Gestão de Licenças - Status Concluída</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>solicitacao_aprovacao</fullName>
        <field>STATUS__c</field>
        <literalValue>Aguardando aprovação</literalValue>
        <name>Solicitação em Aprovação</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>solicitacao_reprovada</fullName>
        <field>STATUS__c</field>
        <literalValue>Reprovada</literalValue>
        <name>Solicitação Reprovada</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>

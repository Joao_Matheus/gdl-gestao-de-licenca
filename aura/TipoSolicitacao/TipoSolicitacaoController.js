({
	carregaArea : function(component, event, helper) {
		helper.carregarAreas(component,event,helper);
	}, carregarUsuarioArea : function (component, event, helper){
		var licenseComponent = component.find("LicenseComponent");
		licenseComponent.set("v.recordId", component.get("v.solicitacao.AREA__c"));
		licenseComponent.reloadLicenseComponent();		
		helper.carregarUsuarios(component,event,helper);
	}, enableFormulario : function(component, event, helper){
		
		var dadosUsuario = component.get("v.usuarios");
		var usuarioOrigem = component.find("userId").get("v.value");
		var mapUsuario = new Map();
		for(var i in dadosUsuario) {  
			mapUsuario.set(dadosUsuario[i].USUARIO__c, dadosUsuario[i].USUARIO__r);
		}
		var usuario = mapUsuario.get(usuarioOrigem); 
		var solic = component.get("v.solicitacao"); 
		solic.ID_PAPEL_USUARIO__c = usuario.UserRoleId;
		solic.ID_PERFIL_USUARIO__c = usuario.ProfileId; 
		solic.PAPEL_USUARIO__c = usuario.UserRoleId;
		solic.PERFIL_USUARIO__c = usuario.Profile.Name; 
		solic.LICENCA__c = usuario.Profile.UserLicense.Name; 
		component.set("v.habilitaForm", true);
	}
})
({
	carregarAreas : function(component, helper, event) {
        return new Promise($A.getCallback( function(resolve, reject) {
        var action = component.get("c.getAreasCoor");
		action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                resolve(response.getReturnValue());
                component.set('v.areas',response.getReturnValue());
            }else if (state === "ERROR") {
                var errors = retorno.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
          $A.enqueueAction(action);
        }));
    
    }, carregarUsuarios: function (component, event, helper){ 
        return new Promise($A.getCallback( function(resolve, reject) {
        var action = component.get("c.getUserArea");
        action.setParams({ idArea : component.get("v.solicitacao.AREA__c") });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                resolve(response.getReturnValue());
                component.set('v.usuarios', response.getReturnValue());
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        $A.enqueueAction(action);
    }));
    },showSpinner: function (component, spinnerName) {
        var spinner = component.find(spinnerName);
        $A.util.removeClass(spinner, "slds-hide");
    },hideSpinner: function (component, spinnerName) {
        var spinner = component.find(spinnerName); 
        $A.util.addClass(spinner, "slds-hide");
    
    }
})
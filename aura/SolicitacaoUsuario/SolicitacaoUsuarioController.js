({
	doInit: function(component, event, helper){
		
		console.log("PayLoad:: " + JSON.stringify(component.get("v.solicitacao")));
		if(helper.bloqueiaAlteracao(component)){
			helper.showSpinner(component, 'loadingPage');
			component.set("v.disableField",true); 
			helper.getRoleByParam(component,helper);
			helper.getProfileByParam(component,helper);
			helper.chargeGerencia(component,helper);
			helper.chargeSuperintendencia(component,helper); 
			helper.chargePermissionSet(component,helper); 
		}		 
		  
		if(helper.validaListasObjetos(component)){ 
			helper.chargeRole(component,helper);
			helper.chargeProfile(component,helper);
			helper.chargeGerencia(component,helper);
			helper.chargeSuperintendencia(component,helper);
			helper.chargePermissionSet(component,helper);
		}   

	},handleChange: function (component, event, helper) {
		var permissionSetList = event.getParam("value");
	},criarSolicitacao : function (component, event, helper){
		helper.getRoleByParam(component, helper);
		helper.getProfileByParam(component, helper);
		
		if(helper.validateFields(component, helper) === true ){
        	helper.showSpinner(component, 'loadingPage'); 
            helper.criarSolicitacao(component, event, helper);
        } 
		
	},alteracaoProfileRole : function(component, event, helper){ 
	
		if(helper.bloqueiaAlteracao(component)){
			component.set("v.disableField",true);
			helper.getRoleByParam(component,helper);
			helper.getProfileByParam(component,helper);
		}		 
 	}
})
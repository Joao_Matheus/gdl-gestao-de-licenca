({
	chargeRole : function(component, event, helper) {
	     var action = component.get("c.getRoles");
	 	 action.setCallback(this, function(response) {
	 	 	var state = response.getState();
	 	 	if (state === "SUCCESS") {
	 	 		var retorno = response.getReturnValue();
	 	 		if(retorno != null && retorno != ''){
	 	 			component.set("v.listRole", retorno);
	 	         }
             }else if (state === "ERROR") {
                  var errors = response.getError();
             if (errors) { 
                 if (errors[0] && errors[0].message) {
                     console.log("Error message: " + errors[0].message);
                 }
             } 
         }
     });
	 $A.enqueueAction(action); 
},chargeProfile : function(component, event, helper) {
	    var action = component.get("c.getProfileCommunity");
	 	 action.setCallback(this, function(response) {
	 	 	var state = response.getState();
            if (state === "SUCCESS") {
	 	 		var retorno = response.getReturnValue();
	 	 		if(retorno != null && retorno != ''){
	 	 			component.set("v.listProfile", retorno);
	 	         }
             }else if (state === "ERROR") {
                  var errors = response.getError();
             if (errors) {
                 if (errors[0] && errors[0].message) {
                     console.log("Error message: " +
                              errors[0].message);
                 }
             }
         }
     });
	 $A.enqueueAction(action);

    }, chargeGerencia : function(component, event, helper){

            var action = component.get("c.carregarGerencia");
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var retorno = response.getReturnValue();
                    if(retorno != null && retorno != ''){
                        component.set("v.listGerencia", retorno);
                    }
            }else if (state === "ERROR") {
                    var errors = response.getError();
            if (errors) {
                if (errors[0] && errors[0].message) {
                    console.log("Error message: " +
                                errors[0].message);
                }
            } 
         }
        });
    $A.enqueueAction(action);

    }, chargeSuperintendencia : function(component, event, helper){
            var action = component.get("c.carregarSuperintendencia");
            action.setCallback(this, function(response) {
                var state = response.getState();
                if(state === "SUCCESS"){
                    var retorno = response.getReturnValue();
                    if(retorno != null && retorno != ''){
                        component.set("v.listSuperintendencia", retorno);
                    }
                }else if (state === "ERROR") {
                    var errors = response.getError();
                 if (errors) {
                        if (errors[0] && errors[0].message) {
                             console.log("Error message: " +
                                errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
    });
    $A.enqueueAction(action);
   },chargePermissionSet : function(component, event, helper){
            var action = component.get("c.getPermissionSets");
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var retorno = response.getReturnValue();
                    if(retorno != null && retorno != ''){
                        var options = [];
                        retorno.forEach(function(retorno)  { 
                            options.push({ value: retorno.Id, label: retorno.Label});
                        });
                        component.set("v.permissionSets", options);
                    }
            } else if (state === "ERROR") {
                    var errors = response.getError();
            if (errors) {
                if (errors[0] && errors[0].message) {
                    console.log("Error message: " +
                                errors[0].message);
                }
            } else {
                console.log("Unknown error");
            }
        } 
        });
        $A.enqueueAction(action);
    
    },validaListasObjetos :function(component, helper){
        return (component.get("v.solicitacao.ID_PAPEL_USUARIO__c") == '' && component.get("v.solicitacao.ID_PERFIL_USUARIO__c") == '') ? true :false;
    },bloqueiaAlteracao : function(component, helper){
        return (component.get("v.solicitacao.ID_PAPEL_USUARIO__c") != '' && component.get("v.solicitacao.ID_PERFIL_USUARIO__c") != '') ? true:false;
    }, criarSolicitacao: function (component, event, helper){ 
        component.set("v.solicitacao.STATUS__c","Nova"); 
        
        var action = component.get("c.criarSolicitacaoUsuario");
        var solicitacao = component.get("v.solicitacao");

        action.setParams({ request : solicitacao });        
        action.setCallback(this, function(response) {

        var state = response.getState();
            if (state === "SUCCESS") {
                helper.hideSpinner(component, 'loadingPage');
                var retornoo = response.getReturnValue();
                if(retornoo.isSuccessfull === false){
                    this.showToast(component, event, helper, 'Erro', retornoo.errorMessage, 'error', 'error');   
                }else{
                    this.showToast(component, event, helper, ':)', 'Solicitação criada com sucesso.', 'success', 'check');
                   	this.redirect(component, event, helper, retornoo.recordId);
                }
            }else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } 
                }
        });
        
        $A.enqueueAction(action);   
    }, getRoleByParam : function(component,event,helper){
        var action = component.get("c.getRoleById");
        action.setParams({id : component.get("v.solicitacao.ID_PAPEL_USUARIO__c")});        
        action.setCallback(this, function(response){
            var state = response.getState(); 
            if (state === "SUCCESS") {
                var retorno = response.getReturnValue(); 
                component.set("v.listRole", retorno);
                component.set("v.solicitacao.PAPEL_USUARIO__c", retorno[0].Name);
            }else if (state === "ERROR") {
                   var errors = response.getError();
            if (errors) { 
                if (errors[0] && errors[0].message) {
                  console.log("Error message: " + errors[0].message);
                }
            } else {
                console.log("Unknown error");
            }
     }

        });
        $A.enqueueAction(action);
    }, getProfileByParam : function(component, event, helper){
        var action = component.get("c.getProfileById");
        action.setParams({id : component.get("v.solicitacao.ID_PERFIL_USUARIO__c")});        
        action.setCallback(this, function(response){
            var state = response.getState(); 
            if (state === "SUCCESS") {
                var spinner = component.find('loadingPage'); 
                $A.util.addClass(spinner, "slds-hide");
               var retorno = response.getReturnValue(); 
                component.set("v.listProfile", retorno);
                component.set("v.solicitacao.PERFIL_USUARIO__c", retorno[0].Name);
            }else if (state === "ERROR") {
                   var errors = response.getError();
            if (errors) {
                if (errors[0] && errors[0].message) {
                  console.log("Error message: " + errors[0].message);
                }
            } else {
                console.log("Unknown error");
            }
     }
        });
        $A.enqueueAction(action);
   
    },validateFields: function (component, helper){
        var result = true;
        if(helper.validateNameField(component) === false){
        	result = false;
        }
        if(helper.validateLastNameField(component) === false){
            result = false; 
        }
        if(helper.validateEmailField(component) === false){
            result = false;
        } 
        if(helper.validateGerenteField(component) === false){
           result = false; 
        }
        if(helper.validateSuperField(component) === false){
            result = false;
        }
        if(helper.validateUserNameField(component) === false){
            result = false;
        }
        
        return result;
    },validateNameField: function (component){
        var nomeUsuario = component.get("v.solicitacao.NOME_USUARIO__c");
        var nameUsuario = component.find("nameUsuario");
        var result = true;
        nameUsuario.set("v.errors", null);
        if(nomeUsuario === '' || nomeUsuario === null){
            nameUsuario.set("v.errors", [{message: "Valor obrigatório"}]);
            result = false; 
        }
		return result;
    },validateLastNameField: function (component){
        var sobrenome = component.get("v.solicitacao.SOBRENOME_USUARIO__c");
        var lastNameSolicitacao = component.find("lastNameSolicitacao");
        var result = true;
        if(sobrenome === null || sobrenome === ''){
            lastNameSolicitacao.set("v.errors", [{message: "Valor obrigatório"}]);
            result = false; 
        }
        return result;
   
    }, validateEmailField: function (component){
        var email = component.get("v.solicitacao.EMAIL_USUARIO__c");
        var emailSolicitacao = component.find("emailSolicitacao");
        var result = true;
        if(email=== null || email === ''){
           emailSolicitacao.set("v.errors", [{message: "Valor obrigatório"}]);
           result = false;
        }
        return result;
    },validateUserNameField: function (component){
        var userName = component.get("v.solicitacao.USERNAME__c");
        var userNameSolicitacao = component.find("userNameSolicitacao");
        var result = true;
        if(userName === null || userName === ''){
           userNameSolicitacao.set("v.errors", [{message: "Valor obrigatório"}]);
           result = false;
        }
        return result;
    },validateGerenteField: function (component){
        var gerenciaUsuario = component.get("v.solicitacao.GERENCIA_USUARIO__c");
        var gerenciaId = component.find("gerenciaId");
        var result = true;
        
        if(gerenciaUsuario === null || gerenciaUsuario === ''){
           gerenciaId.set("v.errors", [{message: "Valor obrigatório"}]);
           result = false;
        }
        return result;
    },validateSuperField: function (component){
        var superintendencia = component.get("v.solicitacao.SUPERINTENDENCIA_USUARIO__c");
        var superintenciaId = component.find("superintenciaId");
        var result = true;
        if(superintendencia === null ||superintendencia === ''){
           superintenciaId.set("v.errors", [{message: "Valor obrigatório"}]);
           result = false;
        }
        return result;
    },showSpinner: function (component, spinnerName) {
        var spinner = component.find(spinnerName);
        $A.util.removeClass(spinner, "slds-hide");
    },hideSpinner: function (component, spinnerName) {
        var spinner = component.find(spinnerName); 
        $A.util.addClass(spinner, "slds-hide");
    }, showToast : function(component, event, helper, title, message, type, icon) {
        var toastEvent = $A.get("e.force:showToast");
        console.log('toast', toastEvent);
        toastEvent.setParams({
            "message": message,
            "type": type,
            "key": icon
        });
        toastEvent.fire();
    }, redirect: function (component, event, helper, recordId) {
        var navEvt = $A.get("e.force:navigateToSObject");
        console.log(recordId);
        navEvt.setParams({
          "recordId": recordId,
          "slideDevName": "Detail"
        });
        navEvt.fire();
    }
})
({
	"getLicenseList" : function(cmp, helper) {
        return new Promise($A.getCallback( function(resolve, reject) {
            console.log('test');
            var action = cmp.get("c.getLicenseListApex");
            console.log('test2');
            action.setParams({ recordId : cmp.get("v.recordId") });
            console.log('componente: ' + cmp.get("v.recordId"));
            action.setCallback(this, function(response) {
                console.log("From server: " + response);
                var state = response.getState();
                if (state === "SUCCESS") {
                    console.log(response);
                    var retorno = response.getReturnValue();
                    resolve(retorno);
                }
                else if (state === "INCOMPLETE") {
                    // do something
                }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    reject(erros);
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                     errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            });
    
            $A.enqueueAction(action);
        }));
    },
    
    "showSpinner": function (component, spinnerName) {
        var spinner = component.find(spinnerName);
        $A.util.removeClass(spinner, "slds-hide");
    },
    
    "hideSpinner": function (component, spinnerName) {
        var spinner = component.find(spinnerName);
        $A.util.addClass(spinner, "slds-hide");
    }
})
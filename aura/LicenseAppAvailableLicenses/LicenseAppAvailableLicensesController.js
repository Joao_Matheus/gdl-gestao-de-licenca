({
	getLicenseList: function (component, event, helper) {
		component.set('v.licenseColumns', [
                {label: 'Licença', fieldName: 'nome', type: 'text'},
                {label: 'Comprada', fieldName: 'quantidadeComprada', type: 'text'},
                {label: 'Utilizada', fieldName: 'quantidadeAtribuida', type: 'text'},
                {label: 'Disponível', fieldName: 'quantidadeDisponivel', type: 'text'}
            ]);
		helper.showSpinner(component, "loadingLicencas");
        helper.getLicenseList(component, helper).then(function(result){
            if(result.isSuccessful){
                component.set('v.licenseData', result.itens);
            }else{
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Licenças",
                    "message": result.errorMessage
                });
                toastEvent.fire();
            }
         	helper.hideSpinner(component, "loadingLicencas");
        });    
    }
})
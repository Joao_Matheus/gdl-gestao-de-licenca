public with sharing class GDLDisponibLicencasAura {
	public GDLDisponibLicencasAura() {}

	@AuraEnabled
	public static GDLDisponibLicencasListResponse getLicenseListApex(Id recordId){
		GDLDisponibLicencasListResponse response = new GDLDisponibLicencasListResponse();
		GDLDisponibLicencasService service = new GDLDisponibLicencasService();
		system.debug(recordId);
		response = service.getLicenceList(recordId);
		return response;
	}
}
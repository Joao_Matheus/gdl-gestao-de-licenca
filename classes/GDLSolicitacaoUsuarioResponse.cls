public with sharing class GDLSolicitacaoUsuarioResponse {

	@AuraEnabled public Boolean isSuccessfull;
	@AuraEnabled public String recordId;
	@AuraEnabled public String errorMessage;

    public GDLSolicitacaoUsuarioResponse() {
		this.isSuccessfull = true;
		this.errorMessage = '';
		this.recordId = null;
	}
}
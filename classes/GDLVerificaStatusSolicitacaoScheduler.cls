global class GDLVerificaStatusSolicitacaoScheduler implements Schedulable{
    global void execute(SchedulableContext sc) { 
		GDLGestaoUsuariosService service = new GDLGestaoUsuariosService();
		service.verificarStatusSolicitacao();
	} 
}
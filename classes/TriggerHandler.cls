public virtual class TriggerHandler {

    @TestVisible private Boolean beforeInsert = false;
	@TestVisible private Boolean beforeUpdate = false;
    @TestVisible private Boolean beforeDelete = false;
	@TestVisible private Boolean afterInsert = false;
	@TestVisible private Boolean afterUpdate = false;
    @TestVisible private Boolean afterDelete = false;
    @TestVisible private Boolean afterUndelete = false;
    
    protected virtual void beforeInsert() {}
    protected virtual void beforeUpdate() {}
    protected virtual void beforeDelete() {}
    protected virtual void afterInsert() {}
    protected virtual void afterUpdate() {}
    protected virtual void afterDelete() {}
    protected virtual void afterUndelete() {}
    
    protected TriggerHandler() {
        if (Trigger.isExecuting) {
            this.beforeInsert = Trigger.isBefore && Trigger.isInsert;
            this.beforeUpdate = Trigger.isBefore && Trigger.isUpdate;
            this.beforeDelete = Trigger.isBefore && Trigger.isDelete;
            this.afterInsert = Trigger.isAfter && Trigger.isInsert;
            this.afterUpdate = Trigger.isAfter && Trigger.isUpdate;
            this.afterDelete = Trigger.isAfter && Trigger.isDelete;
            this.afterUndelete = Trigger.isBefore && Trigger.isUndelete;
        }
    }
     
    public void run() {
        if (this.beforeInsert) {
            beforeInsert();
        }
        else if (this.beforeUpdate) {
            beforeUpdate();
        }
        else if (this.beforeDelete) {
            beforeDelete();
        }
        else if (this.afterInsert) {
            afterInsert();
        }
        else if (this.afterUpdate) {
            afterUpdate();
        }
        else if (this.afterDelete) {
            afterDelete();
        }
        else if (this.afterUndelete) {
            afterUndelete();
        }
    }
}
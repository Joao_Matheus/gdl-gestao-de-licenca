public with sharing class GLDGestaoUsuariosAURA {

    @AuraEnabled
    public static List<AREA__c> getAreasCoor() {
        GDLGestaoUsuariosService service = new GDLGestaoUsuariosService();
        return service.getArea();
    }

    @AuraEnabled
    public static List<AREA_USUARIO__c> getUserArea(id idArea) {
        GDLGestaoUsuariosService service = new GDLGestaoUsuariosService();
        return [Select USUARIO__c, USUARIO__r.Name,USUARIO__r.ProfileId, USUARIO__r.Profile.Name, USUARIO__r.UserRoleId,USUARIO__r.UserRole.Name, USUARIO_APROVADOR__c, ORDEM_APROVACAO__c, AREA__c, Id From AREA_USUARIO__c where AREA__c =: idArea];
    }
}
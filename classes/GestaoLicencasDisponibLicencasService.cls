public with sharing class GestaoLicencasDisponibLicencasService {
	public GestaoLicencasDisponibLicencasService() {}


	public GestaoLicencasLicenseListResponse getLicenceList(Id recordId){
		GestaoLicencasLicenseListResponse response = new GestaoLicencasLicenseListResponse();

		try{
			if(recordId != null && String.valueOf(recordId.getSobjectType()) == 'EQUIPE__c'){
				List<Equipe__c> equipe = [select id, name, (select id, LICENCA__c, LICENCA__r.Id, LICENCA__r.Name, QUANTIDADE__c from ALOCACOES__r order by LICENCA__r.Name), (select Id, USUARIO__r.Profile.UserLicense.Name from ALOCACOES_USUARIO__r where PAGADOR__c =: true AND USUARIO__r.isActive =: true) from Equipe__c where id=: recordId];
				if(equipe.size() > 0){
					Map<String, Integer> mapaQuantidadeComprada = new Map<String, Integer>();
					Map<String, Integer> mapaQuantidadeUtilizada = new Map<String, Integer>();
					for(ALOCACAO__c alocacao: equipe[0].ALOCACOES__r){
						if(mapaQuantidadeComprada.get(alocacao.LICENCA__r.Name) == null){
							mapaQuantidadeComprada.put(alocacao.LICENCA__r.Name, Integer.ValueOf(alocacao.QUANTIDADE__c));
						}else{
							Integer aux = mapaQuantidadeComprada.get(alocacao.LICENCA__r.Name);
							aux += Integer.valueOf(alocacao.QUANTIDADE__c);
							mapaQuantidadeComprada.put(alocacao.LICENCA__r.Name, aux);
						}
					}

					for(ALOCACAO_USUARIO__c alocacao: equipe[0].ALOCACOES_USUARIO__r){
						if(mapaQuantidadeUtilizada.get(alocacao.USUARIO__r.Profile.UserLicense.Name) == null){
							mapaQuantidadeUtilizada.put(alocacao.USUARIO__r.Profile.UserLicense.Name, 1);
						}else{
							Integer aux = mapaQuantidadeUtilizada.get(alocacao.USUARIO__r.Profile.UserLicense.Name);
							aux += 1;
							mapaQuantidadeUtilizada.put(alocacao.USUARIO__r.Profile.UserLicense.Name, aux);
						}
					}
					for(ALOCACAO__c alocacao: equipe[0].ALOCACOES__r){
						GestaoLicencasItemLicenca item = new GestaoLicencasItemLicenca();
						item.nome = alocacao.LICENCA__r.Name;
						item.quantidadeComprada = String.valueOf(mapaQuantidadeComprada.get(alocacao.LICENCA__r.Name));
						item.quantidadeDisponivel = (mapaQuantidadeUtilizada.get(alocacao.LICENCA__r.Name) != null) ? String.valueOf(Integer.valueOf(item.quantidadeComprada) - mapaQuantidadeUtilizada.get(alocacao.LICENCA__r.Name)) : item.quantidadeComprada;
						item.quantidadeAtribuida = (mapaQuantidadeUtilizada.get(alocacao.LICENCA__r.Name) != null) ? String.valueOf(mapaQuantidadeUtilizada.get(alocacao.LICENCA__r.Name)) : '0';
						response.itens.add(item);
					}
				}
			}else if(recordId != null && String.valueOf(recordId.getSobjectType()) == 'AREA__c'){
				List<Equipe__c> equipes = [select id, name, (select id, LICENCA__c, LICENCA__r.Id, LICENCA__r.Name, QUANTIDADE__c from ALOCACOES__r order by LICENCA__r.Name), (select Id, USUARIO__r.Profile.UserLicense.Name from ALOCACOES_USUARIO__r where PAGADOR__c =: true AND USUARIO__r.isActive =: true) from Equipe__c where RESPONSAVEL__r.AREA__c=: recordId];
				if(equipes.size() > 0){
					Map<String, Integer> mapaQuantidadeComprada = new Map<String, Integer>();
					Map<String, Integer> mapaQuantidadeUtilizada = new Map<String, Integer>();
					for(EQUIPE__c equipe: equipes){
						for(ALOCACAO__c alocacao: equipe.ALOCACOES__r){
							if(mapaQuantidadeComprada.get(alocacao.LICENCA__r.Name) == null){
								mapaQuantidadeComprada.put(alocacao.LICENCA__r.Name, Integer.ValueOf(alocacao.QUANTIDADE__c));
							}else{
								Integer aux = mapaQuantidadeComprada.get(alocacao.LICENCA__r.Name);
								aux += Integer.valueOf(alocacao.QUANTIDADE__c);
								mapaQuantidadeComprada.put(alocacao.LICENCA__r.Name, aux);
							}
						}
					}
					for(EQUIPE__c equipe: equipes){
						for(ALOCACAO_USUARIO__c alocacao: equipe.ALOCACOES_USUARIO__r){
							if(mapaQuantidadeUtilizada.get(alocacao.USUARIO__r.Profile.UserLicense.Name) == null){
								mapaQuantidadeUtilizada.put(alocacao.USUARIO__r.Profile.UserLicense.Name, 1);
							}else{
								Integer aux = mapaQuantidadeUtilizada.get(alocacao.USUARIO__r.Profile.UserLicense.Name);
								aux += 1;
								mapaQuantidadeUtilizada.put(alocacao.USUARIO__r.Profile.UserLicense.Name, aux);
							}
						}
					}
					
					for(String key: mapaQuantidadeComprada.KeySet()){
						GestaoLicencasItemLicenca item = new GestaoLicencasItemLicenca();
						item.nome = key;
						item.quantidadeComprada = String.valueOf(mapaQuantidadeComprada.get(key));
						item.quantidadeDisponivel = (mapaQuantidadeUtilizada.get(key) != null) ? String.valueOf(Integer.valueOf(item.quantidadeComprada) - mapaQuantidadeUtilizada.get(key)) : item.quantidadeComprada;
						item.quantidadeAtribuida = (mapaQuantidadeUtilizada.get(key) != null) ? String.valueOf(mapaQuantidadeUtilizada.get(key)) : '0';
						response.itens.add(item);
					}
				}
			}

		}catch(Exception e){
			response.isSuccessful = false;
			response.errorMessage = e.getMessage();
		}

		return response;
	}
}
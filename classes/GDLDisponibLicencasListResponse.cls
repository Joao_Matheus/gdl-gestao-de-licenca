public with sharing class GDLDisponibLicencasListResponse {

	@AuraEnabled public Boolean isSuccessful {get;set;}
	@AuraEnabled public String errorMessage {get;set;}
	@AuraEnabled public List<GDLItemLicenca> itens {get;set;}

	public GDLDisponibLicencasListResponse() {
		this.isSuccessful = true;
		this.errorMessage = ''; 
		this.itens = new List<GDLItemLicenca>();
	}
}
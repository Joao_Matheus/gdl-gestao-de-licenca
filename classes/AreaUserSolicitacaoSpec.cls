public  class AreaUserSolicitacaoSpec extends CompositeSpecification{
    public String idArea;
    
    public AreaUserSolicitacaoSpec(String idArea) {
		this.idArea = idArea;
	} 
	public override Boolean isSatisfiedBy(Object candidate) { 
    	AREA_USUARIO__c areaUsuario = (AREA_USUARIO__c) candidate;
		  return areaUsuario == areaUsuario;
    }

    public override String toSOQLClauses() { 
        return 'AREA__c =  \'' +  idArea + '\'' + ' AND USUARIO_APROVADOR__c != true AND  USUARIO__r.Profile.Name NOT IN (\'' + 'Administrador de Sistema' + '\', \'' + 'System Administrator' + '\')'; 
    }
}
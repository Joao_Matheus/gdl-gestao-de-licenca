public with sharing class SolicitacaoUsuarioFactory {
    public SolicitacaoUsuarioFactory(){}

    public SOLICITACAO__c atribuirAprovadores(SOLICITACAO__c solicitacao, List<AREA_USUARIO__c> aprovadores){
        if( aprovadores.size() > 0 ){
            for(AREA_USUARIO__c a: aprovadores){ 
                solicitacao.APROVADOR_1__c = (a.ORDEM_APROVACAO__c == 1) ? a.USUARIO__c : solicitacao.APROVADOR_1__c;
                solicitacao.APROVADOR_2__c = (a.ORDEM_APROVACAO__c == 2) ? a.USUARIO__c : solicitacao.APROVADOR_2__c;
                solicitacao.APROVADOR_3__c = (a.ORDEM_APROVACAO__c == 3) ? a.USUARIO__c : solicitacao.APROVADOR_3__c;
                solicitacao.APROVADOR_4__c = (a.ORDEM_APROVACAO__c == 4) ? a.USUARIO__c : solicitacao.APROVADOR_4__c;
            }
        }
        return solicitacao; 
    }
}
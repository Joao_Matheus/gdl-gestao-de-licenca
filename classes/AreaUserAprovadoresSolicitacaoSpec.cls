public  class AreaUserAprovadoresSolicitacaoSpec extends CompositeSpecification{ 
  
  public String areaUsuario;
    
  public AreaUserAprovadoresSolicitacaoSpec(String areaUsuario) {
		this.areaUsuario = areaUsuario;
	} 
  
	public override Boolean isSatisfiedBy(Object candidate) { 
    AREA_USUARIO__c areaUsuario = (AREA_USUARIO__c) candidate;
		return areaUsuario == areaUsuario;
  }

    public override String toSOQLClauses() {
        return  'AREA__c =  \'' +  areaUsuario + '\' AND USUARIO_APROVADOR__c = true AND ORDEM_APROVACAO__c != null AND ORDEM_APROVACAO__c <= 4'; 
    } 

}
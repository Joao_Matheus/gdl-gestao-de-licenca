public with sharing class GestaoLicencasItemLicenca {
	
	@AuraEnabled Public String nome {get;set;}
	@AuraEnabled Public String quantidadeComprada {get;set;}
	@AuraEnabled Public String quantidadeAtribuida {get;set;}
	@AuraEnabled Public String quantidadeDisponivel {get;set;}

	public GestaoLicencasItemLicenca() {
		this.nome = '';
		this.quantidadeDisponivel = '0';
		this.quantidadeAtribuida = '0';
		this.quantidadeComprada = '0';
	}
}
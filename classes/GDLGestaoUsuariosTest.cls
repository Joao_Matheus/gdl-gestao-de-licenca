@isTest
public  class GDLGestaoUsuariosTest {
 
    @isTest
  static void deve_criar_iniciativas_caso_dados_sejam_validos() {
     
         BU__c bu = new BU__c(Name = 'Saúde');
         insert bu;
 
         LICENCA_ALOCACAO__c licencasAloc  = new LICENCA_ALOCACAO__c(Name = 'Salesforce' , QUANTIDADE__c = 100);
         insert licencasAloc;
         LICENCA_ALOCACAO__c licencasAloc2  = new LICENCA_ALOCACAO__c(Name = 'Customer Community Plus Login' , QUANTIDADE__c = 100);
         insert licencasAloc2;

         AREA__c area = new AREA__c(Name = 'Engajamento Médico');
         insert area;
         system.debug(area); 

         RESPONSAVEL__c resp  = new RESPONSAVEL__c(Name = 'Marcos Valeriano', AREA__c = area.id);
         insert resp;
         system.debug(resp);

         EQUIPE__c equipe = new EQUIPE__c(Name = 'Engajemento', DATA_FINAL_VIGENCIA__c = System.Today().addDays(10), DATA_INICIO__c = System.Today(), RESPONSAVEL__c = resp.id);
         insert equipe;
         system.debug(equipe);

         ALOCACAO__c alocacoes = new ALOCACAO__c(LICENCA__c = licencasAloc.id, QUANTIDADE__c = 11, EQUIPE__c = equipe.id );
         insert alocacoes;
         system.debug(alocacoes); 

        SOLICITACAO__c solic = new SOLICITACAO__c();
        solic.ID_PAPEL_USUARIO__c = '00E3100000189Z2EAI';
        solic.ID_PERFIL_USUARIO__c = '00e0Z000001RZImQAO';
        solic.SUPERINTENDENCIA_USUARIO__c = 'GEPME';
        solic.GERENCIA_USUARIO__c = 'GEOPS';
        solic.LICENCA__c = 'Salesforce';
        solic.STATUS__c = 'Nova'; 
        solic.TIPO_SOLICITACAO__c =  'Criação de usuário';
        //solic.USUARIO_ORIGEM__c = '005g0000006VWE9AAO';
        solic.AREA__c =  area.id;
        solic.PAPEL_USUARIO__c = 'Supervisor Atendimento';
        //solic.PERFIL_USUARIO__c =  'Atendente - Central de Relacionamento';
        solic.NOME_USUARIO__c = 'asdasdsanasdabjhsdbjahb';
        solic.SOBRENOME_USUARIO__c = 'jhsabdjsahbdjashbdjahb';
        //solic.EMAIL_USUARIO__c = 'jmpinheiro11@gmail.com';
        solic.USERNAME__c = 'adadadsa@hotmail.com'; 
        solic.DEPARTAMENTO_USUARIO__c = 'asdasdas';
        solic.DIVISAO_USUARIO__c = 'asdasd';  
        insert solic;
        Test.startTest();

            GDLUserFactory fac = new GDLUserFactory();
            fac.userFactory(solic);
            GDLGestaoUsuariosService service = new GDLGestaoUsuariosService();
            GDLFormularioSolicitacaoAura.getRoles();
            GDLFormularioSolicitacaoAura.getProfiles(); 
            GDLFormularioSolicitacaoAura.getPermissionSets();
            GDLFormularioSolicitacaoAura.carregarGerencia();
            GDLFormularioSolicitacaoAura.carregarSuperintendencia();
            GDLGestaoUsuariosAURA.getAreasCoor();
            GDLDisponibLicencasAura.getLicenseListApex(area.id);
            GDLFormularioSolicitacaoAura.criarSolicitacaoUsuario(solic);
            service.getUsuarios(area.id);
            GDLGestaoUsuariosAURA.getUserArea(area.id);
            SOLICITACAO__c updateSolic = new SOLICITACAO__c(ID = solic.id, STATUS__c = 'Aprovada');
            Update updateSolic;
        Test.stopTest(); 
  }
}
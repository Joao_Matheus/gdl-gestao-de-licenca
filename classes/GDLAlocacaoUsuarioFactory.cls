public with sharing class GDLAlocacaoUsuarioFactory {

    public GDLAlocacaoUsuarioFactory(){}

     public ALOCACAO_USUARIO__c alocacaoUsuarioFactory(User user, id areaId){
        ALOCACAO_USUARIO__c newAlocacaoUsuario = new ALOCACAO_USUARIO__c();
        newAlocacaoUsuario.EQUIPE__c = areaId;
        newAlocacaoUsuario.PAGADOR__c = true;
        newAlocacaoUsuario.USUARIO__c = user.id; 
        return newAlocacaoUsuario; 
    }

    public List<ALOCACAO_USUARIO__c> alocacaoUsuarioFactoryList(List<User> listUser, Map<id, EQUIPE__c> mapEquipe, Map<id, SOLICITACAO__c> mapSolic){
        List<ALOCACAO_USUARIO__c> listAlocacoesUser = new List<ALOCACAO_USUARIO__c>();
        for(User u : listUser){
            id areaId = mapSolic.get(u.idSolicitacao__c).AREA__c;
            listAlocacoesUser.add(alocacaoUsuarioFactory(u, mapEquipe.get(areaId).id));
        }
        return listAlocacoesUser;
    }

}
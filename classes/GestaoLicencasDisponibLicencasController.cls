public with sharing class GestaoLicencasDisponibLicencasController {
	public GestaoLicencasDisponibLicencasController() {}

	@AuraEnabled
	public static GestaoLicencasLicenseListResponse getLicenseListApex(Id recordId){
		GestaoLicencasLicenseListResponse response = new GestaoLicencasLicenseListResponse();
		GestaoLicencasDisponibLicencasService service = new GestaoLicencasDisponibLicencasService();
		system.debug(recordId);
		response = service.getLicenceList(recordId);
		return response;
	}
}
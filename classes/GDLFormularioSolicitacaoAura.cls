public with sharing class GDLFormularioSolicitacaoAura {

    @AuraEnabled
    public static List<UserRole> getRoles() {
        GDLGestaoUsuariosService service = new GDLGestaoUsuariosService();
        return service.getPapeis();
    }

    @AuraEnabled
    public static GDLSolicitacaoUsuarioResponse criarSolicitacaoUsuario(SOLICITACAO__c request){
        GDLGestaoUsuariosService service = new GDLGestaoUsuariosService();
        return service.criacaoSolicitacaoUsuario(request);
    }
     
    @AuraEnabled 
    public static List<Profile> getProfiles() {
        GDLGestaoUsuariosService service = new GDLGestaoUsuariosService();
        return service.getPerfis();
    }

    @AuraEnabled 
    public static List<PermissionSet> getPermissionSets() {
        GDLGestaoUsuariosService service = new GDLGestaoUsuariosService();
        return service.getPermissionSet(); 
    }

    @AuraEnabled
    public static List<String> carregarGerencia(){
        GDLGestaoUsuariosService service = new GDLGestaoUsuariosService();
        return service.getGerencia();
    }

    @AuraEnabled
    public static List<String> carregarSuperintendencia(){
        GDLGestaoUsuariosService service = new GDLGestaoUsuariosService();
        return service.getSuperintendencia();    
    }
 
    @AuraEnabled
    public static List<UserRole> getRoleById(String id ){
        return [SELECT ID, NAME FROM USERROLE WHERE ID =: id];
    }

    @AuraEnabled
    public static List<Profile> getProfileById(String id ){
        return [SELECT ID, NAME FROM Profile WHERE ID =: id];
    }

    @AuraEnabled
    public static List<Profile> getProfileCommunity(String id ){
        return [Select id ,Name, Profile.UserLicense.Name  From Profile where Profile.UserLicense.Name IN  ('Partner Community','Customer Community Plus Login','Company Communities' )];
    }
    
}
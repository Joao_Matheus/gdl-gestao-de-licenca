public without sharing class GDLCriacaoUsuarioQueue implements Queueable {
     
    public List<SOLICITACAO__c> solicitacoes; 
    public GDLGestaoUsuariosService service;
    public Set<id> idUsers; 
    public GDLCriacaoUsuarioQueue(List<SOLICITACAO__c> solicitacoes){
        this.solicitacoes = solicitacoes;
        this.idUsers = new Set<id>();
        this.service = new GDLGestaoUsuariosService();
    }
    
    public void execute(QueueableContext context) {
        List<Database.SaveResult> rs = new List<Database.SaveResult>();
        if (!Test.isRunningTest()){
             rs = service.criarUsuariosNew(this.solicitacoes);
        }            
        Map<id, SOLICITACAO__c> mapSolic = new Map<id, SOLICITACAO__c>(this.solicitacoes);
            System.Debug('DatabaseResults, Teste ::::  ' + rs);
            if(rs.size() > 0 ){
                for (Database.SaveResult sr : rs) {
                    if (sr.isSuccess()) {
                        idUsers.add(sr.getId());
                    } 
                }
            }
        GDLGestaoUsuariosService.verificarStatusSolicitacao();  
    }
}
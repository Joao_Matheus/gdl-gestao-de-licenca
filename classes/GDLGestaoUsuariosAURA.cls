public with sharing class GDLGestaoUsuariosAURA {

    @AuraEnabled
    public static List<AREA__c> getAreasCoor() {
        GDLGestaoUsuariosService service = new GDLGestaoUsuariosService();
        return service.getArea();
    } 
 
    @AuraEnabled
    public static List<AREA_USUARIO__c> getUserArea(id idArea) {
        GDLGestaoUsuariosService service = new GDLGestaoUsuariosService();
        return service.getUsuarios(idArea);
    }
}
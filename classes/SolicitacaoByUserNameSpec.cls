public class SolicitacaoByUserNameSpec extends CompositeSpecification {
    public String userName;
    
    public SolicitacaoByUserNameSpec(String userName) {
		this.userName = userName;
	} 
	public override Boolean isSatisfiedBy(Object candidate) { 
    	SOLICITACAO__c solicitacao = (SOLICITACAO__c) candidate;
		return solicitacao == solicitacao;
    }

    public override String toSOQLClauses() {
        return 'STATUS__c != \'Concluída\' AND USERNAME__c = \'' + this.userName + '\''; 
    }  

}
public with sharing class GDLItemLicenca {
	
	@AuraEnabled Public String nome {get;set;}
	@AuraEnabled Public String quantidadeComprada {get;set;}
	@AuraEnabled Public String quantidadeAtribuida {get;set;}
	@AuraEnabled Public String quantidadeDisponivel {get;set;}

	public GDLItemLicenca() {
		this.nome = '';
		this.quantidadeDisponivel = '0';
		this.quantidadeAtribuida = '0';
		this.quantidadeComprada = '0';
	}
}
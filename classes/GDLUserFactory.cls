public class GDLUserFactory {

    public GDLUserFactory(){}

    public User userFactory(SOLICITACAO__c solic,User  userOrigin){
        User newUser = new User();
        newUser = userOrigin.clone();
        newUser.isActive = true;
        newUser.UserName = solic.USERNAME__c;
        newUser.FirstName = solic.NOME_USUARIO__c;
        newUser.LastName = solic.SOBRENOME_USUARIO__c;
        newUser.Email = solic.EMAIL_USUARIO__c;
        newUser.Alias = GDLSolicitacaoHelper.createUserAlias(newUser.FirstName, newUser.LastName);
        newUser.ProfileId = solic.ID_PERFIL_USUARIO__c;
        newUser.UserRoleId = solic.ID_PAPEL_USUARIO__c;
        newUser.Superintendencia__c = solic.SUPERINTENDENCIA_USUARIO__c;
        newUser.Gerencia__c = solic.GERENCIA_USUARIO__c;
        newUser.IdSolicitacao__c = solic.Id;
        if(solic.CONTATO__c != null ){
            newUser.ContactId = solic.CONTATO__c;
            newUser.CommunityNickname = solic.EMAIL_USUARIO__c !=null ? solic.EMAIL_USUARIO__c.substring(0, solic.EMAIL_USUARIO__c.indexOf('@')) +  Datetime.now().getTime() : '';
            newUser.UserRoleId = null;
        }
        newUser.TimeZoneSidKey = 'America/Sao_Paulo';
        newUser.LocaleSidKey =  'pt_BR';
        newUser.EmailEncodingKey =  'ISO-8859-1';
        newUser.LanguageLocaleKey = 'pt_BR';
        System.debug('FactoryUser:::: ' + newUser);
        return newUser; 
    } 

     public User userFactory(SOLICITACAO__c solic){
        User newUser = new User();
        newUser.isActive = true;  
        newUser.UserName = solic.USERNAME__c;
        newUser.FirstName = solic.NOME_USUARIO__c;
        newUser.LastName = solic.SOBRENOME_USUARIO__c;
        newUser.Email = solic.EMAIL_USUARIO__c;
        newUser.Alias = GDLSolicitacaoHelper.createUserAlias(newUser.FirstName, newUser.LastName);
        newUser.ProfileId = solic.ID_PERFIL_USUARIO__c; 
        newUser.UserRoleId = solic.ID_PAPEL_USUARIO__c;
        newUser.Superintendencia__c = solic.SUPERINTENDENCIA_USUARIO__c;
        newUser.Gerencia__c = solic.GERENCIA_USUARIO__c;
        newUser.IdSolicitacao__c = solic.Id;
        if(solic.CONTATO__c != null ){
            newUser.ContactId = solic.CONTATO__c;
            newUser.CommunityNickname = solic.EMAIL_USUARIO__c !=null ? solic.EMAIL_USUARIO__c.substring(0, solic.EMAIL_USUARIO__c.indexOf('@')) +  Datetime.now().getTime() : '';
            newUser.UserRoleId = null;         
        }
        newUser.TimeZoneSidKey = 'America/Sao_Paulo';
        newUser.LocaleSidKey =  'pt_BR';
        newUser.EmailEncodingKey =  'ISO-8859-1';
        newUser.LanguageLocaleKey = 'pt_BR';
        return newUser; 
    } 

    public List<User> userFactoryList(List<SOLICITACAO__c> listSolic, Map<id, User> mapUserOrigin){
        List<User> listUsers = new List<User>();
        
        for(SOLICITACAO__c s : listSolic){
           if(mapUserOrigin.get(s.USUARIO_ORIGEM__c) != null && s.LICENCA__c == 'Salesforce'){
              listUsers.add(userFactory(s, mapUserOrigin.get(s.USUARIO_ORIGEM__c) ));
           }
           
           if(mapUserOrigin.get(s.USUARIO_ORIGEM__c) != null && s.LICENCA__c != 'Salesforce'){
              listUsers.add(userFactoryCommunity(s, mapUserOrigin.get(s.USUARIO_ORIGEM__c) ));
           }
        }
        return listUsers; 
    }

     public List<User> userFactoryList(List<SOLICITACAO__c> listSolic){
        List<User> listUsers = new List<User>();
        
        for(SOLICITACAO__c s : listSolic){
              listUsers.add(userFactory(s));
        }
        return listUsers;
    }


    public User userFactoryCommunity(SOLICITACAO__c solic,User  userOrigin){
        User newUser = new User();
        Database.DMLOptions dmo = new Database.DMLOptions();

        newUser.isActive = true;
        newUser.UserName = solic.USERNAME__c;
        newUser.FirstName = solic.NOME_USUARIO__c;
        newUser.LastName = solic.SOBRENOME_USUARIO__c;
        newUser.Email = solic.EMAIL_USUARIO__c;
        newUser.Alias = GDLSolicitacaoHelper.createUserAlias(newUser.FirstName, newUser.LastName);
        newUser.ProfileId = solic.ID_PERFIL_USUARIO__c;
        newUser.Superintendencia__c = solic.SUPERINTENDENCIA_USUARIO__c;
        newUser.Gerencia__c = solic.GERENCIA_USUARIO__c;
        newUser.IdSolicitacao__c = solic.Id;
        if(solic.CONTATO__c != null ){
            newUser.ContactId = solic.CONTATO__c;
            newUser.CommunityNickname = solic.EMAIL_USUARIO__c !=null ? solic.EMAIL_USUARIO__c.substring(0, solic.EMAIL_USUARIO__c.indexOf('@')) +  Datetime.now().getTime() : '';
            newUser.UserRoleId = null;
        }
        newUser.TimeZoneSidKey = 'America/Sao_Paulo';
        newUser.LocaleSidKey =  'pt_BR'; 
        newUser.EmailEncodingKey =  'ISO-8859-1';
        newUser.LanguageLocaleKey = 'pt_BR';
        dmo.EmailHeader.triggerUserEmail = false;       
        dmo.EmailHeader.triggerOtherEmail = false;
        dmo.EmailHeader.triggerAutoResponseEmail = false;       
        dmo.optAllOrNone = false;
        System.debug('FactoryUser:::: ' + newUser);
        newUser.setOptions(dmo);

        return newUser; 

    }
}
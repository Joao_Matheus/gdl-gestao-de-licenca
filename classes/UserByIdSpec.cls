public with sharing class UserByIdSpec extends CompositeSpecification {
    public Set<id> usersId;
    public String ids;

    public UserByIdSpec(Set<id> usersId) {
		this.usersId = usersId;
    this.ids = '(';

		Integer cont = 0;
	
		for(id t : usersId){
			cont++;
			this.ids += '\'' + t + '\'';
			this.ids += (cont == usersId.size()) ? ')' : ',';
		}


	} 
	public override Boolean isSatisfiedBy(Object candidate) { 
      return (candidate == null ? false :  true);
  }

    public override String toSOQLClauses() {
        return 'id IN' + this.ids ; 
    } 
}
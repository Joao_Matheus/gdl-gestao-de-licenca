public class GDLAtribuicaoPermissionSetQueue implements Queueable {
     
    public List<PermissionSetAssignment> PermisionSetCreate ; 
        public  List<GroupMember> GroupMemberCreate ;  

    public GDLAtribuicaoPermissionSetQueue( List<PermissionSetAssignment> PermisionSetCreate,List<GroupMember> GroupMemberCreate){
        this.PermisionSetCreate = PermisionSetCreate;
        this.GroupMemberCreate = GroupMemberCreate;
        
    }
    public void execute(QueueableContext context) {
        
        if( PermisionSetCreate.size() > 0){
            insert PermisionSetCreate;
        }

        if (!Test.isRunningTest()){
            if(GroupMemberCreate.size() > 0){
                GDLAtribuicaoGroupQueue queueGroup = new GDLAtribuicaoGroupQueue(GroupMemberCreate);
                System.enqueueJob(queueGroup);    
            }
        }
    }

}
public with sharing class UserByUserNameSpec extends CompositeSpecification {
    public String userName;
    
    public UserByUserNameSpec(String userName) {
		this.userName = userName;
	} 
	public override Boolean isSatisfiedBy(Object candidate) { 
    User usuario = (User) candidate;
		return usuario == usuario;
  }

    public override String toSOQLClauses() {
        return 'username = \'' + this.userName + '\''; 
    } 
}
public with sharing class GDLSolicitacaoHandler extends TriggerHandlerPattern{

  private List<SOLICITACAO__c> listNew;
  private List<SOLICITACAO__c> listOld;
  private Map<id, SOLICITACAO__c> mapNew;
  private Map<id, SOLICITACAO__c> mapOld;
  private GDLSolicitacaoHelper helper;
  private IRepositoryFactory repositoryFactory; 

public GDLSolicitacaoHandler(){
    this.repositoryFactory = new RepositoryFactory();

}

public GDLSolicitacaoHandler(List<SOLICITACAO__c> listNew, List<SOLICITACAO__c> listOld, Map<id, SOLICITACAO__c> mapNew ,Map<id, SOLICITACAO__c> mapOld){
        this.listNew = listNew;
        this.listOld = listOld;
        this.mapNew = mapNew;
        this.mapOld = mapOld;
        this.helper = new GDLSolicitacaoHelper(); 
        this.repositoryFactory = new RepositoryFactory();
} 

public override void beforeDelete(){}
public override void afterDelete(){}
public override void afterUndelete(){}


  public override void beforeInsert(){}
    
  public override void beforeUpdate(){
    this.validarLicencasSolicitacoes();
    this.criacaoUsuarioSolicitacao();
    this.criarAlocacao();
  }

  public override void afterInsert(){
    this.enviarProcessoAprovacao();
  }

  public override void afterUpdate(){
  }

  public void enviarProcessoAprovacao(){
        for(SOLICITACAO__c solic : this.listNew){   
            if(this.helper.criterioProcessoAprovacao(solic)){// VERIFICAR COMO ENVIAR EM LISTA DE APROVAÇÂO
                Approval.ProcessSubmitRequest request = new Approval.ProcessSubmitRequest();
                request.setComments('Envio para o processo de aprovação');
                request.setObjectId(solic.id); 
                Approval.ProcessResult result = Approval.process(request);
            }
        }
    }

  public void criacaoUsuarioSolicitacao(){// REFATORAR METODO
        List<SOLICITACAO__c>  solic = new List<SOLICITACAO__c>();
        
        for(SOLICITACAO__c s: listNew){
            if(this.helper.criterioEnviarCriacaoUsuario(s, mapOld.get(s.Id).STATUS__c)){
                solic.add(s);
                s.STATUS__c = 'Processando';
            }
        }

        if(solic.size() > 50){
            throw new GDLSolicitacaoException('Você não pode aprovar mais que 50 registros de solicitação ao mesmo tempo');
        }

        if(solic.size() > 0){
            GDLCriacaoUsuarioQueue queue = new GDLCriacaoUsuarioQueue(solic);
            System.enqueueJob(queue);
        }
    }


    public void validarLicencasSolicitacoes(){// REFATORAR METODO 
		
        List<Id> idsAreas = new List<Id>();
        List<String> idsPerfis = new List<String>();
		Map<id, Equipe__c> mapEquipe = new Map<id, Equipe__c>();
        
        for(SOLICITACAO__c s: listNew){
	    	idsAreas.add(s.AREA__c);
		}
        
        for(Equipe__c e: [SELECT id, name, RESPONSAVEL__r.AREA__c, 
                         (select id, LICENCA__c, LICENCA__r.Id, LICENCA__r.Name, QUANTIDADE__c from ALOCACOES__r  order by LICENCA__r.Name), 
                         (select Id, USUARIO__r.Profile.UserLicense.Name from ALOCACOES_USUARIO__r where PAGADOR__c = true AND USUARIO__r.isActive = true) 
                         FROM Equipe__c where RESPONSAVEL__r.AREA__c IN: idsAreas order by RESPONSAVEL__r.AREA__r.Id]){
            mapEquipe.put(e.RESPONSAVEL__r.AREA__c, e);
        }

        for(SOLICITACAO__c s: listNew ){
            if(this.helper.validaLicensaDisponivel(s, mapEquipe)){
		    	s.STATUS__c = 'Aprovada - aguardando licença';
            }
        }
	}

    public void criarAlocacao(){ 
        GDLAlocacaoUsuarioFactory factory = new GDLAlocacaoUsuarioFactory();
        Set<id> idAreas = new Set<id>();
        Set<id> idSolicitacoes = new Set<id>();
        Map<id, EQUIPE__c> mapEquipe = new Map<id, EQUIPE__c>();

        for(SOLICITACAO__c s : this.mapNew.values()){
            if(this.helper.validaCriterioAlocacao(s, this.mapOld)){ 
                idAreas.add(s.AREA__c);
                idSolicitacoes.add(s.id);
            }
        }
        
        if(this.helper.validaListaCriterioAlocacao(idAreas, idSolicitacoes)){ 
            for(EQUIPE__c e : [SELECT id, name, RESPONSAVEL__r.AREA__c FROM Equipe__c where RESPONSAVEL__r.AREA__c IN: idAreas]){
                mapEquipe.put(e.RESPONSAVEL__r.AREA__c, e);
            }

            List<User> listUsers = [Select id, idSolicitacao__c from User where idSolicitacao__c in: idSolicitacoes];
            if(listUsers.size() > 0){
                List<ALOCACAO_USUARIO__c> listAlocacaoUser = factory.alocacaoUsuarioFactoryList(listUsers, mapEquipe, mapNew);
                if(listAlocacaoUser.size() > 0){
                    insert listAlocacaoUser;
                }
            }	
        }
	}
}
public with sharing class GestaoLicencasLicenseListResponse {
	@AuraEnabled public Boolean isSuccessful {get;set;}
	@AuraEnabled public String errorMessage {get;set;}
	@AuraEnabled public List<GestaoLicencasItemLicenca> itens {get;set;}

	public GestaoLicencasLicenseListResponse() {
		this.isSuccessful = true;
		this.errorMessage = '';
		this.itens = new List<GestaoLicencasItemLicenca>();
	}

}
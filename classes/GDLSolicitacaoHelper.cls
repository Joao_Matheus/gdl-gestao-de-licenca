public  class GDLSolicitacaoHelper {
 
    public GDLSolicitacaoHelper(){}
 
    public boolean criterioProcessoAprovacao(SOLICITACAO__c solicitacao){
        return (solicitacao.STATUS__c == 'Nova' && solicitacao.TIPO_SOLICITACAO__c ==  'Criação de Usuário' ? true : false);
    }

    public boolean criterioEnviarCriacaoUsuario(SOLICITACAO__c solicitacao,String oldStatus){ 
        return (solicitacao.STATUS__c == 'Aprovada' && oldStatus != 'Aprovada' && solicitacao.TIPO_SOLICITACAO__c == 'Criação de Usuário' ? true : false);
    }

    public static string createUserAlias(String firstName, String lastName){ 
		String response = (firstName.length() >= 3) ? firstName.substring(0,2) : firstName;
		response += '.';
		response += (lastName.length() >= 3) ? lastName.substring(0,2) : lastName;
		List<User> users = [Select id from User where Alias =: response];
		if(users.size() > 0){
			response+= users.size(); 
		}
		return response;
	}

    public boolean validaCriterioAlocacao(SOLICITACAO__c solic, Map<id, SOLICITACAO__c> mapOld){
        return (solic.STATUS__c == 'Concluída' && mapOld.get(solic.id).STATUS__c != 'Concluída' && solic.AREA__c != null ? true: false);
    }

    public boolean validaListaCriterioAlocacao(Set<id> idAreas,Set<id> idSolicitacoes){
        return (idAreas.size() > 0 && idSolicitacoes.size() > 0  ? true: false); 
    }

    public boolean validaLicensaDisponivel(SOLICITACAO__c solic, Map<id, Equipe__c> mapEquipe){
        Map<string, List<ALOCACAO_USUARIO__c>> mapAlocacaoUser = new Map<string, List<ALOCACAO_USUARIO__c>>(); 
        Map<string, ALOCACAO__c> mapAlocacaoLicenca = new Map<string, ALOCACAO__c>();
            
        if(mapEquipe.get(solic.AREA__c) != null && solic != null) { 
                for(ALOCACAO_USUARIO__c au : mapEquipe.get(solic.AREA__c).ALOCACOES_USUARIO__r ){
                    if(mapAlocacaoUser.containsKey(au.USUARIO__r.Profile.UserLicense.Name)) {
                        List<ALOCACAO_USUARIO__c> alocsUser = mapAlocacaoUser.get(au.USUARIO__r.Profile.UserLicense.Name);
                        alocsUser.add(au);
                        mapAlocacaoUser.put(au.USUARIO__r.Profile.UserLicense.Name, alocsUser);
                    } else {
                        mapAlocacaoUser.put(au.USUARIO__r.Profile.UserLicense.Name, new List<ALOCACAO_USUARIO__c> {au});
                    }
                }
                for(ALOCACAO__c a : mapEquipe.get(solic.AREA__c).ALOCACOES__r){
                       mapAlocacaoLicenca.put(a.LICENCA__r.Name, a);
                }
                if(mapAlocacaoUser.get(solic.LICENCA__c) != null && mapAlocacaoLicenca.get(solic.LICENCA__c) != null ){
                    return (mapAlocacaoUser.get(solic.LICENCA__c).size() >= mapAlocacaoLicenca.get(solic.LICENCA__c).QUANTIDADE__c && solic.STATUS__c == 'Aprovada' ? true: false);
                }
        }
        return false;  
    } 
        

}
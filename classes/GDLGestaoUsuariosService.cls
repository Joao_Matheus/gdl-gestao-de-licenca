public without sharing class GDLGestaoUsuariosService { 

    public GDLGestaoUsuariosHelper helper; 
    private IRepositoryFactory repositoryFactory; 

    public GDLGestaoUsuariosService (){ 
        this.helper = new GDLGestaoUsuariosHelper ();
        this.repositoryFactory = new RepositoryFactory();
    }
    public List<AREA__c> getArea(){
        IRepository repository = repositoryFactory.create(AREA__c.sObjectType);         
        Repository.QueryBuilder queryBuilder = new Repository.QueryBuilder()
        .selectFields(FieldSetUtil.getFieldsFromFieldSet(AREA__c.SObjectType, 'AREA'));
        List<AREA__c> areas = (List<AREA__c>) repository.queryObjects(queryBuilder);
        return areas;
    } 

    public List<AREA_USUARIO__c> getUsuarios(String idArea){
        List<String> fields = new List<String>();
        fields.add('USUARIO__c, USUARIO__r.Name,USUARIO__r.ProfileId, USUARIO__r.Profile.UserLicense.Name, USUARIO__r.Profile.Name, USUARIO__r.UserRoleId, USUARIO__r.UserRole.Name,USUARIO_APROVADOR__c, ORDEM_APROVACAO__c, AREA__c, Id');
        IRepository repository = repositoryFactory.create(AREA_USUARIO__c.sObjectType);         
        Specification spec = new AreaUserSolicitacaoSpec(idArea);
        Repository.QueryBuilder queryBuilder = new Repository.QueryBuilder()
        //.selectFields(FieldSetUtil.getFieldsFromFieldSet(AREA_USUARIO__c.SObjectType, 'AREA_USUARIO'))
        .selectFields(fields)
        .whereSpecification(spec);
        List<AREA_USUARIO__c> listaUsuarios = (List<AREA_USUARIO__c>) repository.queryObjects(queryBuilder);
        return listaUsuarios;  
    }
 
    public List<User> getUser(Set<id> usersId){
        List<String> fields = FieldSetUtil.getFieldsFromFieldSet(User.SObjectType, 'USUARIO_GDL');
        fields.add('IsPortalEnabled,UserPermissionsSupportUser,UserPermissionsSFContentUser,CallCenterId,UserPermissionsLiveAgentUser,UserPermissionsKnowledgeUser, IsProfilePhotoActive');
        IRepository repository = repositoryFactory.create(User.sObjectType);            
        Specification spec = new UserByIdSpec(usersId);
        Repository.QueryBuilder queryBuilder = new Repository.QueryBuilder()
        .selectFields(fields)
        .whereSpecification(spec);
        return (List<User>) repository.queryObjects(queryBuilder);    
    }

    public List<UserRole> getPapeis(){//Mudar para o Padrão 
        return [Select Id, Name From UserRole where PortalType = 'None' order by Name];
    }

    public List<UserRole> getPapeisById(){//Mudar para o Padrão 
        return [Select Id, Name From UserRole where PortalType = 'None' order by Name];
    }
    
    public List<Profile> getPerfis(){
        IRepository repository = repositoryFactory.create(Profile.sObjectType);         
        Repository.QueryBuilder queryBuilder = new Repository.QueryBuilder()
        .selectFields(new List<String>{'Id','Name'}) 
        .orderBy(new List<String>{'Name'}); 
        return (List<Profile>)repository.queryObjects(queryBuilder); 
    }

     public List<User> getUserExistente(String userName){
        IRepository repository = repositoryFactory.create(User.sObjectType);         
        Specification spec = new UserByUserNameSpec(userName);
        Repository.QueryBuilder queryBuilder = new Repository.QueryBuilder()
        .selectFields(FieldSetUtil.getFieldsFromFieldSet(User.SObjectType, 'USUARIO_GDL'))
        .whereSpecification(spec);
        List<User> listUsers = (List<User>) repository.queryObjects(queryBuilder);
        return listUsers;
    } 

    public List<SOLICITACAO__c> getSolicitacaoExistente(String userName){
        IRepository repository = repositoryFactory.create(SOLICITACAO__c.sObjectType);         
        Specification spec = new SolicitacaoByUserNameSpec(userName);
        Repository.QueryBuilder queryBuilder = new Repository.QueryBuilder()
        .selectFields(FieldSetUtil.getFieldsFromFieldSet(SOLICITACAO__c.SObjectType,'GESTAO_LICENCA'))
        .whereSpecification(spec);
        List<SOLICITACAO__c> listSolicitacoes = (List<SOLICITACAO__c>) repository.queryObjects(queryBuilder);
        return listSolicitacoes;
    }
    
    public List<AREA_USUARIO__c> getUsuariosAprovadores(String idArea){
        IRepository repository = repositoryFactory.create(AREA_USUARIO__c.sObjectType);         
        Specification spec = new AreaUserAprovadoresSolicitacaoSpec(idArea);
        Repository.QueryBuilder queryBuilder = new Repository.QueryBuilder()
        .selectFields(FieldSetUtil.getFieldsFromFieldSet(AREA_USUARIO__c.SObjectType, 'AREA_USUARIO'))
        .whereSpecification(spec);
        List<AREA_USUARIO__c> listaUsuarios = (List<AREA_USUARIO__c>) repository.queryObjects(queryBuilder);
        return listaUsuarios;  
    }

    public List<String> getGerencia(){
         List<String> options = new List<String>();
         Schema.DescribeFieldResult fieldResult = User.Gerencia__c.getDescribe();
         List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
         for( Schema.PicklistEntry f : ple){
            options.add(f.getValue());
         }       
         return options;    
    }

    public List<String> getSuperintendencia(){
         List<String> options = new List<String>();
         Schema.DescribeFieldResult fieldResult = User.Superintendencia__c.getDescribe();
         List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
         for( Schema.PicklistEntry f : ple){
            options.add(f.getValue());
         }       
         return options;    
    }

    public List<PermissionSet> getPermissionSet(){
        IRepository repository = repositoryFactory.create(PermissionSet.sObjectType);         
        Repository.QueryBuilder queryBuilder = new Repository.QueryBuilder()
        .selectFields(new List<String>{'Id','Name', 'Label' }) 
        .orderBy(new List<String>{'Name'}); 
        return (List<PermissionSet>)repository.queryObjects(queryBuilder);
    } 

    public GDLSolicitacaoUsuarioResponse criacaoSolicitacaoUsuario(SOLICITACAO__c request){// CRIAR A FACTORY
        GDLSolicitacaoUsuarioResponse response = new GDLSolicitacaoUsuarioResponse();
        SolicitacaoUsuarioFactory factory = new SolicitacaoUsuarioFactory();
        List<SOLICITACAO__c> listSolic = new List<SOLICITACAO__c>();
        try{
            IRepository repository = repositoryFactory.create(SOLICITACAO__c.sObjectType);            
            this.helper.validateFields(request);
            List<User> users = this.getUserExistente(request.USERNAME__c);
            this.helper.validaExistenciaUsuario(users);
            List<SOLICITACAO__c> solicitacoes = this.getSolicitacaoExistente(request.USERNAME__c);
            this.helper.validaExistenciaSolicitacao(solicitacoes);
     	    List<AREA_USUARIO__c> usuariosAprovadores  = this.getUsuariosAprovadores(request.AREA__c);
            request = factory.atribuirAprovadores(request,usuariosAprovadores);
            listSolic.add(request);
            if(listSolic.size() > 0){
               repository.insertObject(listSolic);
            }
            response.isSuccessfull = true;
     		response.recordId = request.Id;
        
        }catch(Exception e){
	 		response.isSuccessfull = false;
	 		response.errorMessage = e.getMessage();
	 		response.recordId = null;
	 	}

        return response;
    }

   

	public List<Database.SaveResult> criarUsuariosNew(List<SOLICITACAO__c> solicitacoes){
            Set<id> idsUserOrigem = new Set<id>();
            GDLUserFactory factoryUser = new GDLUserFactory();
            for(SOLICITACAO__c s : solicitacoes){
                idsUserOrigem.add(s.USUARIO_ORIGEM__c);
            }
            Map<id, User> userOrigin = new Map<id, User>(this.getUser(idsUserOrigem));
            List<User> users = factoryUser.userFactoryList(solicitacoes, userOrigin);
            List<Database.SaveResult> retornoProcessamento = Database.Insert(users, false);   
            return retornoProcessamento;            
	} 

    @Future
    public static void verificarStatusSolicitacao(){ //VERIFICAR MELHOR FORMA DE IMPLENTAR
		try{ 
			
            List<SOLICITACAO__c> solicitacoes = [select id, STATUS__c, AREA__c, USUARIO_ORIGEM__c, LastModifiedDate FROM SOLICITACAO__c WHERE STATUS__c = 'Processando'];
			Map<id, SOLICITACAO__c> mapSolicitacoes = new Map<id, SOLICITACAO__c>();
            Map<String, User> mapUser = new Map<String, User>();
            Set<id> idUserOrigem = new Set<id>();
            Set<id> idSolicitacoes = new Set<id>();
            List<GroupMember> GroupMemberCreate = new List<GroupMember>();
            List<PermissionSetAssignment> PermisionSetCreate = new List<PermissionSetAssignment>();
            Map<id, Set<id>> mapGroup = new Map<id,Set<id>>();
            Map<id, Set<id>> mapPermissionSet = new Map<id, Set<id>>();

            if(solicitacoes.size() > 0){
                
                 for(SOLICITACAO__c s:solicitacoes ){
                    idUserOrigem.add(s.USUARIO_ORIGEM__c);
                    idSolicitacoes.add(s.id);
                }

			    List<User> users = [Select id, idSolicitacao__c from User where idSolicitacao__c in : idSolicitacoes]; 
				
                for(User u: users){ 
					mapUser.put(u.idSolicitacao__c, u);
                } 
 

                for(GroupMember g: [Select UserOrGroupId,GroupId, Id From GroupMember where UserOrGroupId IN: idUserOrigem]){
                     if(mapGroup.containsKey(g.UserOrGroupId)) {
                        Set<Id> groupIds = mapGroup.get(g.UserOrGroupId);
                        groupIds.add(g.GroupId);
                        mapGroup.put(g.UserOrGroupId, groupIds);
                    } else {
                        mapGroup.put(g.UserOrGroupId, new Set<Id> { g.GroupId });
                    }
                }

                 for(PermissionSetAssignment g: [Select AssigneeId, PermissionSetId From PermissionSetAssignment  where AssigneeId IN: idUserOrigem and PermissionSet.IsOwnedByProfile = false]){
                   if(mapPermissionSet.containsKey(g.AssigneeId)) {
                        Set<Id> permissionSetIds = mapPermissionSet.get(g.AssigneeId);
                        permissionSetIds.add(g.PermissionSetId);
                        mapPermissionSet.put(g.AssigneeId, permissionSetIds);
                    } else {
                        mapPermissionSet.put(g.AssigneeId, new Set<Id> { g.PermissionSetId });
                    }
                } 
			    
                 
                Datetime dataAux = Datetime.now();
				dataAux = dataAux.addMinutes(-10); 
			    for(SOLICITACAO__c s: solicitacoes){  
					s.STATUS__c = (mapUser.get(s.Id) != null) ? 'Concluída' : ((s.LastModifiedDate >= dataAux) ? 'Erro no processamento' : s.STATUS__c);
                    
                    if( mapUser.get(s.Id) != null){
                       if(mapPermissionSet != null && !mapPermissionSet.isEmpty()){ 
                            if( mapPermissionSet.get(s.USUARIO_ORIGEM__c).size() > 0 && mapPermissionSet.get(s.USUARIO_ORIGEM__c) != null ){
                                for(id i: mapPermissionSet.get(s.USUARIO_ORIGEM__c)){
                                
                                    PermissionSetAssignment ps = new PermissionSetAssignment();
                                    system.debug(mapUser.get(s.id));
                                    ps.AssigneeId = mapUser.get(s.id).id;
                                    ps.PermissionSetId = i;
                                    PermisionSetCreate.add(ps);
                                
                                    system.debug(ps);
                                }
                            }
                      }
                   
                    if(mapGroup != null && !mapGroup.isEmpty()){
                      if( mapGroup.get(s.USUARIO_ORIGEM__c).size() > 0 && mapGroup.get(s.USUARIO_ORIGEM__c) != null){
                          for(id i: mapGroup.get(s.USUARIO_ORIGEM__c)){
                              GroupMember gm = new GroupMember();
                              gm.UserOrGroupId = mapUser.get(s.id).id;
                              gm.groupId = i;
                              GroupMemberCreate.add(gm);
                              system.debug(gm);
                          }
                      }
                    }
                    }
                } 
                
                if(PermisionSetCreate.size() > 0){
                    GDLAtribuicaoPermissionSetQueue queuePermissionSet = new GDLAtribuicaoPermissionSetQueue(PermisionSetCreate,GroupMemberCreate );
                    System.enqueueJob(queuePermissionSet);
                }

                update solicitacoes;	
			}
            update solicitacoes;
		
        }catch(Exception e){
			system.debug('#### ' + e.getMessage() + ' - ' + e.getStackTraceString());
		}
	}  
}
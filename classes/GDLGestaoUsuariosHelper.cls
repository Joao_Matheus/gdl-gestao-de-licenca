public with sharing class GDLGestaoUsuariosHelper {

    public GDLGestaoUsuariosHelper (){}

    public void validateFields(SOLICITACAO__c request){
        this.nomeIsValid(request.NOME_USUARIO__c);
        this.sobrenomeIsValid(request.SOBRENOME_USUARIO__c);
        this.emailIsValid(request.EMAIL_USUARIO__c);
        this.gerenciaIsValid(request.GERENCIA_USUARIO__c);
        this.superintendenciaIsValid(request.SUPERINTENDENCIA_USUARIO__c);
        this.usernameIsValid(request.USERNAME__c);
    }

    public void nomeIsValid(String nome){
        if(nome == '' || nome == null){
 		    throw new GDLSolicitacaoException('Campo Nome é obrigatório');
 		}
    }
    
    public void sobrenomeIsValid(String subrenome){
        if(subrenome == '' || subrenome == null){
 		    throw new GDLSolicitacaoException('Campo Sobrenome é obrigatório');
 		}
    }
    
    public void emailIsValid(String email){
        if(email == '' || email == null){
 		    throw new GDLSolicitacaoException('Campo Email é obrigatório');
 		}
    }
    
    public void gerenciaIsValid(String gerencia){
        if(gerencia == '' || gerencia == null){
 		    throw new GDLSolicitacaoException('Campo Gerencia é obrigatório');
 		}
    }

    public void superintendenciaIsValid(String superintendencia){
        if(superintendencia == '' || superintendencia == null){
 		    throw new GDLSolicitacaoException('Campo Superintendencia é obrigatório');
 		}
    }

    public void usernameIsValid(String username){
        if(username == '' || username == null){
 		    throw new GDLSolicitacaoException('Campo Nome de usuário é obrigatório');
 		}
    }

    public void validaExistenciaUsuario(List<User> listUser){
        if(listUser.size() > 0){
            throw new GDLSolicitacaoException('Usuário já cadastrado');
        }
    }

    public void validaExistenciaSolicitacao(List<SOLICITACAO__c> listSolicitacao){
	    if(listSolicitacao.size()>0){
            throw new GDLSolicitacaoException('Já existe uma solicitação para a criação deste usuário em andamento.');
        }
    }
}
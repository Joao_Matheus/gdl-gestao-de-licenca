#!/usr/bin/python
# coding: utf-8
import sys
import re
import os
import shutil
from simple_salesforce import Salesforce

sf = Salesforce(username=sys.argv[1], password=sys.argv[2], security_token=sys.argv[3], client_id=sys.argv[4], sandbox=False)
sf_component_request = []
payload = {
    "ambiente": sys.argv[5] 
}
sf_component_response = sf.apexecute('CIChangeComponentsStatusRS/changeComponentsStatusRS', method='POST', data=payload)
exit()
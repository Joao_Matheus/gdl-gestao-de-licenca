#!/usr/bin/python
# coding: utf-8
import sys
import re
import os
import shutil
from shutil import copyfile, copytree
import codecs
import zipfile
import xml.etree.ElementTree as ET
from simple_salesforce import Salesforce

def namespace(element):
    m = re.match('\{.*\}', element.tag)
    return m.group(0) if m else ''
    
def getfile(type, member, files, actualStatus):
    configs  = {
	'AuthProvider': [['authproviders','authprovider']],
    'CustomApplication': [['applications','app']],
    'AppMenu': [['appMenus','appMenu']],
    'ApprovalProcess': [['approvalProcesses','approvalProcess']],
    'AssignmentRules': [['assignmentRules','assignmentRules']],
    'AutoResponseRules': [['autoResponseRules','autoResponseRules']],
    'ApexClass': [['classes','cls'],['classes', 'cls-meta.xml']],
    'Community': [['communities','community']],
    'ApexComponent': [['components','component'], ['components', 'component-meta.xml']],
    'CustomApplicationComponent': [['customApplicationComponents','customApplicationComponent']],
    'DataCategoryGroup': [['datacategorygroups','datacategorygroup']],
    'EntitlementProcess': [['entitlementProcesses','entitlementProcess']],
    'EscalationRules': [['escalationRules','escalationRules']],
    'Fields': [['customFields']],
    'Flow': [['flows','flow']],
    'Group': [['groups','group']],
    'HomePageComponent': [['homePageComponents','homePageComponent']],
    'HomePageLayout': [['homePageLayouts','homePageLayout']],
   'CustomLabel' : [['labels', 'labels']],
    'CustomLabels': [['labels','labels']],
    'Layout': [['layouts','layout']],
    'Letterhead': [['letterhead','letter']],
    'LiveChatAgentConfig': [['liveChatAgentConfigs','liveChatAgentConfig']],
    'LiveChatButton': [['liveChatButtons','liveChatButton']],
    'LiveChatDeployment': [['liveChatDeployments','liveChatDeployment']],
    'MilestoneType': [['milestoneTypes','milestoneType']],
    'Network': [['networks','network']],
    'CustomObject': [['objects','object']],
    'CustomObjectTranslation': [['objectTranslations','objectTranslation']],
    'ApexPage': [['pages','page'], ['pages', 'page-meta.xml']],
	'PathAssistant': [['pathAssistants','pathAssistant']],
    'PermissionSet': [['permissionsets','permissionset']],
    'Profile': [['profiles','profile']],
    'Queue': [['queues','queue']],
    'QuickAction': [['quickActions','quickAction']],
    'RemoteSiteSetting': [['remoteSiteSettings','remoteSite']],
    'ReportType': [['reportTypes','reportType']],
    'Role': [['roles','role']],
    'Skill': [['skills','skill']],
    'Settings': [['settings','settings']],
    'SiteDotCom': [['siteDotComSites','site'], ['siteDotComSites', 'site-meta.xml']],
    'CustomSite': [['sites','site']],
    'StaticResource': [['staticresources','resource'], ['staticresources', 'resource-meta.xml']],
    'CustomTab': [['tabs','tab']],
    'CustomMetadata': [['customMetadata','md']],
    'ApexTrigger': [['triggers','trigger'],['triggers','trigger-meta.xml']],
    'CustomPageWebLink': [['weblinks','weblink']],
    'Workflow': [['workflows','workflow']],
    'WorkflowAlert': [['workflows', 'workflow']],
    'WorkflowFieldUpdate': [['workflows', 'workflow']],
    'WorkflowTask': [['workflows', 'workflow']],
    'WorkflowRule': [['workflows', 'workflow']],
    'WebLink': [['weblinks', 'weblink']],
    'RemoteSiteSettings': [['remoteSiteSettings', 'remoteSite']],
    'Report': [['reports', 'report']],
    'Dashboard': [['dashboards', 'dashboard']],
    'Document': [['documents', '-meta.xml']],
    'EmailTemplate': [['email', 'email'], ['email', 'email-meta.xml']],
    'SharingCriteriaRule': [['sharingRules', 'sharingRules']],
    'SharingRules': [['sharingRules', 'sharingRules']],
    'GlobalPicklist': [['globalPicklists', 'globalPicklist']],
    'FlowDefinition': [['flowDefinitions', 'flowDefinition']],
    'FlexiPage': [['flexipages', 'flexipage']],
    'AuraDefinitionBundle': [['aura']],
    'ConnectedApp': [['connectedApps', 'connectedApp']],
    'GlobalValueSet': [['globalValueSets', 'globalValueSet']],
    'StandardValueSet': [['standardValueSets', 'standardValueSet']],
    'CallCenter': [['callCenters', 'callCenter']],
    'CustomPermission': [['customPermissions', 'customPermission']],
    'NamedCredential': [['namedCredentials', 'namedCredential']],
	'MatchingRule': [['matchingRules', 'matchingRule']],
	'DuplicateRule': [['duplicateRules', 'duplicateRule']],
	'ContentAsset': [['contentassets', 'asset'], ['contentassets', 'asset-meta.xml']],
	'WaveApplication': [['wave', 'wapp']],
	'WaveDashboard': [['wave', 'wdash']],
	'WaveDataflow': [['wave', 'wdf']],
	'WaveDataset': [['wave', 'wds']]
    }

    if(type == "CustomField" or type == "ListView" or type == "WebLink" or type == "CustomFeedFilter" or type == "RecordType" or type == "ValidationRule" or type == "BusinessProcess" or type == "CompactLayout"):
        valid = True
        for value in actualStatus:
            if(value == ("objects/"+str(member.split(".")[0]) + ".object")):
                valid = None
        if(valid):
            actualStatus.append("objects/"+str(member.split(".")[0]) + ".object")
            files.append(os.path.join("objects", str(member.split(".")[0]) + ".object"))
            newpath = r'manifest/Deploy/objects' 
            if not os.path.exists(newpath):
                os.makedirs(newpath)
            src = os.path.join('objects', str(member.split(".")[0]) + ".object")
            des = os.path.join('manifest', 'Deploy', 'objects', str(member.split(".")[0]) + ".object")
            copyfile(src, des)
    elif(type == "Dashboard" or type == "Report" or type == "EmailTemplate"):
        for config in configs[type]:
            foldersrc = config[0]
            extension = config[1]
            if not os.path.exists(r'manifest/Deploy/'+foldersrc):
                os.makedirs(r'manifest/Deploy/'+foldersrc)
        if(member.find("/") > 0):
            folder = str(member.split("/")[0])
            if not os.path.exists(r'manifest/Deploy/'+foldersrc+'/'+folder):
                os.makedirs(r'manifest/Deploy/'+foldersrc+'/'+folder)
            for config in configs[type]:
                foldersrc = config[0]
                extension = config[1]
                src = os.path.join(foldersrc, member + "." + extension)
                des = os.path.join('manifest', 'Deploy', foldersrc, member + "." + extension)
                copyfile(src, des)
        else:
            src = os.path.join(foldersrc, member + "-meta.xml")
            des = os.path.join('manifest', 'Deploy', foldersrc, member + "-meta.xml")
            copyfile(src, des)

    elif(type == "Document"):
        for config in configs[type]:
            foldersrc = config[0]
            if not os.path.exists(r'manifest/Deploy/'+foldersrc):
                os.makedirs(r'manifest/Deploy/'+foldersrc)
        if(member.find("/") > 0):
            folder = str(member.split("/")[0])
            if not os.path.exists(r'manifest/Deploy/'+foldersrc+'/'+folder):
                os.makedirs(r'manifest/Deploy/'+foldersrc+'/'+folder)
            for config in configs[type]:
                foldersrc = config[0]
                extension = config[1]
                src = os.path.join(foldersrc, member)
                des = os.path.join('manifest', 'Deploy', foldersrc, member)
                copyfile(src, des)
                src = os.path.join(foldersrc, member + "" + extension)
                des = os.path.join('manifest', 'Deploy', foldersrc, member + "" + extension)
                copyfile(src, des)
        else:
            src = os.path.join(foldersrc, member + "-meta.xml")
            des = os.path.join('manifest', 'Deploy', foldersrc, member + "-meta.xml")
            copyfile(src, des)

    elif(type == "ApprovalProcess"):
        for config in configs[type]:
            foldersrc = config[0]
            extension = config[1]
            if not os.path.exists(r'manifest/Deploy/'+foldersrc):
                os.makedirs(r'manifest/Deploy/'+foldersrc)
        for config in configs[type]:
            foldersrc = config[0]
            extension = config[1]
            src = os.path.join(foldersrc, member + "." + extension)
            des = os.path.join('manifest', 'Deploy', foldersrc, member + "." + extension)
            copyfile(src, des)
    elif (type == "Layout"):
        if(member.find("Case-") > 0):
            print("Layout de Case ignorado no deploy")
        else:
            if(member.find(".") > 0):
               member = str(member.split(".")[0])  
        for config in configs[type]:
            foldersrc = config[0]
            extension = config[1]
            if not os.path.exists(r'manifest/Deploy/'+foldersrc):
                os.makedirs(r'manifest/Deploy/'+foldersrc)
        for config in configs[type]:
            foldersrc = config[0]
            extension = config[1]
            src = os.path.join(foldersrc, member + "." + extension)
            des = os.path.join('manifest', 'Deploy', foldersrc, member + "." + extension)
            copyfile(src, des)
    elif (type == "AuraDefinitionBundle"):
        for config in configs[type]:
            foldersrc = config[0]
            if not os.path.exists(r'manifest/Deploy/'+foldersrc):
                os.makedirs(r'manifest/Deploy/'+foldersrc)
        src = os.path.join(foldersrc, member)
        des = os.path.join('manifest', 'Deploy', foldersrc, member)
        copytree(src, des)
    elif (type == "Flow"):
      if not os.path.exists(r'flows/'+member+'.flow'):
        raise Exception("Arquivo "+member+" NÃ£o existe na pasta flows")
      if(member.find('-') > 0):
        version = int(float(''.join(member.split('-')[-1:])))
        name = ''.join(member.split('-{version}'.format(version=version)))
        sf_flow_component_request.append({"componentName": name, "componentVersion": version})
      else:
        name = ''.join(member.split('-')[-1:])
        sf_flow_component_request.append({"componentName": name, "componentVersion": 1})
    elif (type == "EntitlementProcess"):
      if not os.path.exists(r'entitlementProcesses/'+member+'.entitlementProcess'):
        raise Exception("Arquivo "+member+" NÃ£o existe na pasta entitlementProcesses")
      if(member.find('_') > 0):
        version = int(float(''.join((''.join(member.split('_')[-1:])).split('v')[-1:])))
        name = ''.join(member.split('_v{version}'.format(version=version)))
        sf_ep_component_request.append({"componentName": name, "componentVersion": version})
      else:
        name = ''.join(member.split('_')[-1:])
        sf_ep_component_request.append({"componentName": name, "componentVersion": 1})
    elif (type == "CustomLabel"):
        for config in configs[type]:
            foldersrc = config[0]
            if not os.path.exists(r'manifest/Deploy/'+foldersrc):
                os.makedirs(r'manifest/Deploy/'+foldersrc)
        src = os.path.join(foldersrc, 'CustomLabels.labels')
        des = os.path.join('manifest', 'Deploy', foldersrc, 'CustomLabels.labels')
        copytree(src, des)
    elif (type == "QuickAction"):
      for config in configs[type]:
        foldersrc = config[0]
        extension = config[1]
        if not os.path.exists(r'manifest/Deploy/'+foldersrc):
          os.makedirs(r'manifest/Deploy/'+foldersrc)
        src = os.path.join(foldersrc, member + "." + extension)
        des = os.path.join('manifest', 'Deploy', foldersrc, member + "." + extension)
        copyfile(src, des)
    else:
        if(member.find(".") > 0):
            if(type == "CustomMetadata"):
              member = str(".".join(member.split(".")[0:2]))
            else:  
              member = str(member.split(".")[0])  
        for config in configs[type]:
         foldersrc = config[0]
         extension = config[1]
         if (type == "PermissionSet" or type == "Profile"):
            xmlProfile = ET.parse(os.path.join(foldersrc, member + "." + extension))
            rootProfile = xmlProfile.getroot()
            for child in rootProfile:
               if (child.tag == str(namespace + "userPermissions")):
                  profileErrorMessage = type + " " + member + " contem a tag userPermissions, remova e faca o deploy novamente"
                  raise Exception(profileErrorMessage)
         if not os.path.exists(r'manifest/Deploy/'+foldersrc):
           os.makedirs(r'manifest/Deploy/'+foldersrc)
         src = os.path.join(foldersrc, member + "." + extension)
         des = os.path.join('manifest', 'Deploy', foldersrc, member + "." + extension)
         copyfile(src, des)
    return files

if(len(sys.argv)<2):
    print("** Nome do package nÃ£o definido ...")
    exit()
   
if(len(sys.argv)<3):
    print("** Workspace indefinido ...")
    exit()
 
print('\n*************** SULAMERICA SALESFORCE PACKER **************************')
print('**        Processamento dinÃ¢mico de pacotes SALESFORCE               **')
print("** Package :  %s     "%sys.argv[1])
print("**")
print("** Carregando %s"%sys.argv[1])

try:
   xml = ET.parse(sys.argv[1])
except IOError:
    print("** Erro: Arquivo %s nÃ£o encontrado.\n** Verifique e tente novamente."%sys.argv[1])
    exit()
    
print('***********************************************************************\n')

if(len(sys.argv) == 8):
  sf = Salesforce(username=sys.argv[3], password=sys.argv[4], security_token=sys.argv[5], client_id=sys.argv[6], sandbox=False)
#Salesforce(username='ci@sulamerica.com.br.devci', password='Password@01', security_token='vu8WWgNBhHIROvMIOhYXbfIiV', client_id='devci', sandbox=True)
sf_flow_component_request = []
sf_ep_component_request = []

ignore = ['CustomLabel']
profileValid = True
profileErrorMessage = ""
root = xml.getroot()
namespace = namespace(root)
print("namespace %s"%namespace)
workspace = sys.argv[2]
newpath = r'manifest/Deploy' 
if not os.path.exists(newpath):
  os.makedirs(newpath)
else:
  shutil.rmtree(newpath)
  os.makedirs(newpath)
src = os.path.join(sys.argv[1])
des = os.path.join('manifest', 'Deploy', 'package.xml')
copyfile(src, des)
files = []
actualStatus = []
for child in root:
  if(child.tag == str(namespace + "types")):
    name = child.find(str(namespace + "name")).text
    if(name in ignore):
      continue
    for member in child:
      if(member.tag == str(namespace + "members")):
        files = getfile(name, member.text.encode('utf-8'), files, actualStatus)
payload = {
    "request": {
      "ambiente": "",    
      "componentGroup": []
    }
}             
if(len(sf_flow_component_request) > 0):
  payload["request"]["componentGroup"].append({"componentType": "Flow", "components": sf_flow_component_request})
if(len(sf_ep_component_request) > 0):
  payload["request"]["componentGroup"].append({"componentType": "EntitlementProcess", "components": sf_ep_component_request})
if(len(payload["request"]["componentGroup"]) > 0 and len(sys.argv) == 8):
  payload["request"]["ambiente"] = sys.argv[7]
  print("request")
  print(payload)
  sf_component_response = sf.apexecute('CIComponentsActiveVersionRS/getComponentsActiveVersion', method='POST', data=payload)
  print("response")
  print(sf_component_response)
  if(sf_component_response['remoteResponse']['isSuccessful'] == True and sf_component_response['componentGroup'] != None):
    xml = ET.parse('manifest/Deploy/package.xml')
    root = xml.getroot()
    for child in root:
      if(child.tag == str(namespace + "types")):
        name = child.find(str(namespace+"name")).text
        if(name == "Flow"):
          for member in child:
            if(member.tag == str(namespace + "members")):  
              for grp in sf_component_response['componentGroup']:
                if(grp["componentType"] == "Flow"):
                  for resp in grp["components"]:
                    new_component_name = "{flow}-{version}".format(flow=resp['componentName'],version=resp['componentVersion'])
                    if(member.text.find('-') > 0):
                      version = int(float(''.join(member.text.split('-')[-1:])))
                      name = ''.join(member.text.split('-{version}'.format(version=version)))
                      old_component_name = "{flow}-{version}".format(flow=resp['componentName'],version=version)
                      if(name == resp['componentName']):
                        member.text = str(new_component_name)
                        foldersrc = 'flows'
                        extension = 'flow'
                        if not os.path.exists(r'manifest/Deploy/'+foldersrc):
                          os.makedirs(r'manifest/Deploy/'+foldersrc)
                        src = os.path.join(foldersrc, old_component_name + "." + extension)
                        des = os.path.join('manifest', 'Deploy', foldersrc, new_component_name + "." + extension)
                        copyfile(src, des)
                        break
                    else:
                      name = ''.join(member.text.split('-')[-1:])
                      if(name == resp['componentName']):
                        member.text = str(new_component_name)
                        foldersrc = 'flows'
                        extension = 'flow'
                        if not os.path.exists(r'manifest/Deploy/'+foldersrc):
                          os.makedirs(r'manifest/Deploy/'+foldersrc)
                        src = os.path.join(foldersrc, old_component_name + "." + extension)
                        des = os.path.join('manifest', 'Deploy', foldersrc, new_component_name + "." + extension)
                        copyfile(src, des)
                        break
        elif(name == "EntitlementProcess"):
          for member in child:
            if(member.tag == str(namespace + "members")):  
              for grp in sf_component_response['componentGroup']:
                if(grp["componentType"] == "EntitlementProcess"):
                  for resp in grp["components"]:
                    new_component_name = "{ep}_v{version}".format(ep=resp['componentName'].encode("utf-8"),version=resp['componentVersion'])
                    print("new component name")
                    print(new_component_name)
                    member_name = member.text.encode('utf-8')
                    if(member_name.find('_') > 0):
                      version = int(float(''.join((''.join(member_name.split('_')[-1:])).split('v')[-1:])))
                      name = ''.join(member_name.split('_v{version}'.format(version=version)))
                      old_component_name = "{ep}_v{version}".format(ep=resp['componentName'].encode("utf-8"),version=version)
                      if(name == resp['componentName'].encode("utf-8")):
                        member.text = str(new_component_name).decode('utf-8')
                        foldersrc = 'entitlementProcesses'
                        extension = 'entitlementProcess'
                        if not os.path.exists(r'manifest/Deploy/'+foldersrc):
                          os.makedirs(r'manifest/Deploy/'+foldersrc)
                        src = os.path.join(foldersrc, old_component_name + "." + extension)
                        des = os.path.join('manifest', 'Deploy', foldersrc, new_component_name + "." + extension)
                        copyfile(src, des)
                        break
                    else:
                      name = ''.join(member_name.split('_')[-1:])
                      if(name == resp['componentName']):
                        member.text = str(new_component_name).decode('utf-8')
                        foldersrc = 'entitlementProcesses'
                        extension = 'entitlementProcess'
                        if not os.path.exists(r'manifest/Deploy/'+foldersrc):
                          os.makedirs(r'manifest/Deploy/'+foldersrc)
                        src = os.path.join(foldersrc, old_component_name + "." + extension)
                        des = os.path.join('manifest', 'Deploy', foldersrc, new_component_name + "." + extension)
                        copyfile(src, des)
                        break
        else:
          continue
    xml.write('manifest/Deploy/package.xml')
exit()
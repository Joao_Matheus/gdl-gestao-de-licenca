trigger SolicitacaoTrigger on SOLICITACAO__c (before update,before insert, after update,after insert) {
    GDLSolicitacaoHandler solicitacaoHandler = new GDLSolicitacaoHandler(Trigger.New,Trigger.old, Trigger.newMap, Trigger.oldMap);
    solicitacaoHandler.run();
}